<?php
//requiere archivo de la clase externa en es script
require("auto.php");

//titulo
echo "Clase 3 PHP: Instanciar clases externas";

//instacia y configuro los coches
$auto1=new Automovil("Volkswagen", "Vento", "Rojo", "Carlos Rivera");
$auto2=new Automovil("Nissan", "Versa", "Vino", "Lulu Xique");

//Acceder a metodos get y set del objeto 1

echo "
	<strong>".$auto1->getPropietario()."</strong> acaba de comprar un 
	<strong>".$auto1->getMarca()."</strong> modelo 
	<strong>".$auto1->getModelo()."</strong> de color 
	<strong>".$auto1->getColor()."</strong>.
";

//Acceder a metodos get y set del objeto 2

echo "
	<strong>".$auto2->getPropietario()."</strong> acaba de comprar un 
	<strong>".$auto2->getMarca()."</strong> modelo 
	<strong>".$auto2->getModelo()."</strong> de color 
	<strong>".$auto2->getColor()."</strong>.
";

?>